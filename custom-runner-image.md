# Creating a custom runner image with custom hosts IPs

## 1. Create a custom Image

### 1.1 Running the Container
```bash
docker pull ubuntu
# pulling the original version of Ubuntu
docker run -it ubuntu bash
# running a new container using the original version of ubutnu in an interactive mode (-it & bash)
```

### 1.2 Modify the Container
Insdie the container, update the /etc/hosts file
```bash
apt update
apt install nano
nano /etc/hosts
# Add the line 
# ip_address   hostanme
```

### 1.3 Creating the new custom image
After existing the container
```bash
docker commit <container_name> <image_name>:<tag>
```

## 2. Modify the .gitlab-ci.yml file 
Insdie the .gitlab-ci.yml file update the follwoing line

```yaml
image: busybox
# updated to 
image: runner-image:1
```
