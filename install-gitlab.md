# Install Gitlab

```bash

sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl

sudo apt-get install -y postfix

curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash

EXTERNAL_URL="http://localhost"

sudo apt install gitlab-ce

sudo gitlab-ctl reconfigure

```

## Further Confirurations

```bash
sudo nano /etc/gitlab/gitlab.rb
#Basic gitlab server confirgurations
sudo nano /etc/gitlab/initial_root_password
#Password setting
```
