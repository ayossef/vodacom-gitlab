# Install Gitlab Runner

## Download the gitlab runner binary 
https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/index.html

```bash
cd Downloads
curl -O https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/deb/gitlab-runner_amd64.deb
```
## Install the downloaded version using dpkg
```bash
sudo dpkg -i gitlab-runner_amd64.deb
```